package com.example.mader_l.eyecansee;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;



public class MainActivity extends Activity implements   SurfaceHolder.Callback {
    TextView text;
    ImageView im;
    RequestQueue queue;
    Camera mcamera;
    SurfaceView surface;
    SurfaceHolder holder;
    Boolean is_released = false;
    String encodedImage = null;
    String suser = "";
    String stoken = "";
    File f;
    static final int START_CAM = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("", "Lucie");
        text = (TextView) findViewById(R.id.bla);
        text.setTextSize(30);
        im = (ImageView) findViewById(R.id.im);
        surface = (SurfaceView)findViewById(R.id.surface_view);
        queue = Volley.newRequestQueue(this);
        f = new File("/sdcard/download/token.txt");
        if(f.exists() && !f.isDirectory())
            startCam();
        else
        {
            Intent intent = new Intent(getBaseContext(), CheckToken.class);
            startActivityForResult(intent, START_CAM);
        }
    }

    void startCam()
    {
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(f);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            suser = bufferedReader.readLine();
            stoken = bufferedReader.readLine();
            fileReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        holder = surface.getHolder();
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        holder.addCallback(this);
        surface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mcamera.takePicture(null, null, jpegCallback);
            }
        });
    }

    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {

            Bitmap old_bm = BitmapFactory.decodeByteArray(data, 0, data.length);
            text.setText("Sending request to the server...");
            //   surfaceDestroyed(holder);
            // stopPreviewAndFreeCamera();
            if (old_bm.getWidth() > old_bm.getHeight())
                old_bm = Bitmap.createBitmap(old_bm, (old_bm.getWidth() - old_bm.getHeight()) / 2, 0, old_bm.getHeight(), old_bm.getHeight());
            else
                old_bm = Bitmap.createBitmap(old_bm, 0, (old_bm.getHeight() - old_bm.getWidth()) / 2, old_bm.getWidth(), old_bm.getWidth());
            Bitmap bm = Bitmap.createScaledBitmap(old_bm, 224, 224, false);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
            im.setImageBitmap(bm);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            Request("http://scia.epita.fr/eyecansee/api/detect");
        }
    };

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (data.hasExtra("is_finished"))
                startCam();
            else if (data.hasExtra("check_again"))
            {
                Intent intent = new Intent(getBaseContext(), CheckToken.class);
                startActivityForResult(intent, START_CAM);
            }
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {

        try {
            mcamera.setPreviewDisplay(holder);
            mcamera.setDisplayOrientation(90);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Se déclenche quand la surface est détruite
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mcamera != null) {
            // Call stopPreview() to stop updating the preview surface.
          //  mcamera.stopPreview();
        }
    }

    private void stopPreviewAndFreeCamera() {
        if (mcamera != null) {
        //    mcamera.stopPreview();
          //  mcamera.release();
            is_released = true;
        }
    }

    // Se déclenche quand la surface change de dimensions ou de format
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (!is_released)
            mcamera.startPreview();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mcamera = Camera.open();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mcamera.release();
    }

    void Request(final String url)
    {
        Handler handler=new Handler();
        handler.post(new Runnable() {
        public void run () {
            try {
                synchronized (this) {
                    wait(500);
                    runOnUiThread(new Runnable() {

                                      @Override
                                      public void run() {
                                          StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                                  new Response.Listener<String>() {
                                                      @Override
                                                      public void onResponse(String response) {

                                                          try {
                                               if (url.equals("http://scia.epita.fr/eyecansee/api/detect"))
                                                              {
                                                                  JSONObject reader = new JSONObject(response);
                                                                  String txt = reader.getString("text");
                                                                  String audio = reader.getString("audio");
                                                                  text.setText(txt);
                                                                  byte[] decodedBytes = Base64.decode(audio.getBytes("UTF-8"), Base64.DEFAULT);
                                                                  try {
                                                                      File file = new File("/sdcard/download/possible.mp3");
                                                                      file.delete();
                                                                      OutputStream outputStream = new FileOutputStream("/sdcard/download/possible.mp3");
                                                                      ByteArrayOutputStream baos = new ByteArrayOutputStream(decodedBytes.length);
                                                                      baos.write(decodedBytes, 0, decodedBytes.length);
                                                                      baos.writeTo(outputStream);
                                                                      String PATH_TO_FILE = "/sdcard/download/possible.mp3";
                                                                      MediaPlayer mediaPlayer = new MediaPlayer();
                                                                      mediaPlayer.setDataSource(PATH_TO_FILE);
                                                                      mediaPlayer.prepare();
                                                                      mediaPlayer.start();
                                                                      mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                                                                          public void onCompletion(MediaPlayer mp) {
                                                                          }
                                                                      });
                                                                  } catch (IOException e) {
                                                                      e.printStackTrace();
                                                                  }
                                                              }
                                                          } catch (JSONException e) {
                                                              e.printStackTrace();
                                                          } catch (UnsupportedEncodingException e) {
                                                              e.printStackTrace();
                                                          } catch (IOException e) {
                                                              e.printStackTrace();
                                                          }
                                                          Log.d("", response);

                                                      }
                                                  }, new Response.ErrorListener() {
                                              @Override
                                              public void onErrorResponse(VolleyError error) {
                                                  if (error instanceof TimeoutError) {
                                                      text.setText("TimeoutError");
                                                  } else {
                                                      if (error instanceof NoConnectionError) {
                                                          text.setText("NoConnectionError");
                                                      }
                                                  }
                                              }
                                          }
                                          ) {
                                              @Override
                                              protected Map<String, String> getParams() throws AuthFailureError {
                                                  Map<String, String> map = new HashMap<String, String>();
                                                      map.put("user", suser);
                                                      map.put("token", stoken);
                                                      map.put("image", encodedImage);
                                                  return map;
                                              }
                                          };
                                          stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                                                  20000,
                                                  DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                  DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                          queue.add(stringRequest);
                                      }
                                  }
                    );
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        ;
    }
    );
}
    }