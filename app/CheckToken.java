package com.example.mader_l.eyecansee;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;



public class CheckToken extends Activity {
    EditText user;
    EditText token;
    Button save;
    RequestQueue queue;
    Boolean tokenized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.check_token);
        queue = Volley.newRequestQueue(this);
        user = (EditText) findViewById(R.id.user);
        token = (EditText) findViewById(R.id.token);
        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Request("http://scia.epita.fr/eyecansee/api/check_creds");
            }
        });
    }
    public void finish() {
        // Prepare data intent
        Intent data = new Intent();
        if (tokenized)
            data.putExtra("is_finished", "yes");
        else
            data.putExtra("check_again", "yes");
        // Activity finished ok, return the data

        setResult(RESULT_OK, data);
        super.finish();
    }
    void Request(final String url)
    {
        Handler handler=new Handler();
        handler.post(new Runnable() {
                         public void run () {
                             try {
                                 synchronized (this) {
                                     wait(500);
                                     runOnUiThread(new Runnable() {
                                                       @Override
                                                       public void run() {
                                                           StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                                                   new Response.Listener<String>() {
                                                                       @Override
                                                                       public void onResponse(String response) {

                                                                           try {
                                                                               if (url.equals("http://scia.epita.fr/eyecansee/api/check_creds")) {
                                                                                   if (response.equals("1")) {
                                                                                       OutputStream outputStream = new FileOutputStream("/sdcard/download/token.txt");
                                                                                       byte[] userB = user.getText().toString().getBytes();
                                                                                       byte[] tokenB = token.getText().toString().getBytes();
                                                                                       ByteArrayOutputStream baos = new ByteArrayOutputStream(tokenB.length);
                                                                                       baos.write(userB, 0, userB.length);
                                                                                       baos.write("\n".getBytes(), 0, 1);
                                                                                       baos.write(tokenB, 0, tokenB.length);
                                                                                       baos.writeTo(outputStream);
                                                                                       tokenized = true;
                                                                                       finish();
                                                                                   }
                                                                               }
                                                                           }
                                                                           catch (UnsupportedEncodingException e) {
                                                                               e.printStackTrace();
                                                                           } catch (FileNotFoundException e) {
                                                                               e.printStackTrace();
                                                                           } catch (IOException e) {
                                                                               e.printStackTrace();
                                                                           }
                                                                           Log.d("", response);

                                                                       }
                                                                   }, new Response.ErrorListener() {
                                                               @Override
                                                               public void onErrorResponse(VolleyError error) {
                                                                   if (error instanceof TimeoutError) {

                                                                   } else {
                                                                       if (error instanceof NoConnectionError) {
                                                                       }
                                                                   }
                                                               }
                                                           }
                                                           ) {
                                                               @Override
                                                               protected Map<String, String> getParams() throws AuthFailureError {
                                                                   Map<String, String> map = new HashMap<String, String>();
                                                                       map.put("user", user.getText().toString());
                                                                       map.put("token", token.getText().toString());
                                                                   return map;
                                                               }
                                                           };
                                                           stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                                                                   20000,
                                                                   DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                                   DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                                           queue.add(stringRequest);
                                                       }
                                                   }
                                     );
                                 }
                             } catch (InterruptedException e) {
                                 e.printStackTrace();
                             }
                         }
                         ;
                     }
        );
    }
}