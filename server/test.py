#! /usr/bin/env python3

from PIL import Image
import eyecansee

from sentence import multitext

img = Image.open(open("beer-computer336.png", "rb"))
text = multitext(eyecansee.zones_top5(img))

print(text)
