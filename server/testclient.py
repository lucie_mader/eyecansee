#! /usr/bin/env python3

import requests
from base64 import b64encode, b64decode
import sys
from subprocess import Popen, PIPE, DEVNULL
import json
import getpass

with open(sys.argv[1], "rb") as f:
    token = getpass.getpass("Token for testclient: ")
    res = requests.post("http://scia.epita.fr/eyecansee/detect",
            data={"image": b64encode(f.read()), "user": "testclient", "token": token})
    try:
        res = json.loads(res.text)
    except:
        print(res.text)
        sys.exit(1)
    print(res["text"])
    p = Popen(["/usr/bin/cvlc", "--play-and-exit", "-"], stdin=PIPE, stdout=DEVNULL, stderr=DEVNULL)
    p.communicate(b64decode(res["audio"]))
