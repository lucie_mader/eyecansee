def generate_all():
    #Training datas
    call_generates("Train_resized/", "Train_redone/", "train.txt")

    #Validation datas
    call_generates("Validation_resized/", "Validation_redone/", "val.txt")

    #Testing datas
    call_generates("Test_resized/", "Test_redone/", "test.txt")


def call_generates(old_image_dir, new_image_dir, image_file):

    #local_datas
    classes_file = "/home/zeletochoy/eyecansee/server/classes"

    images_file_root = "/home/zeletochoy/caffe/data/ilsvrc12/"
    goinfre_root = "/sgoinfre/eyecansee/"
    res_images_file_root = "/home/zeletochoy/eyecansee/server/images_location/"

    generate_from_ilsvrc(classes_file,
            images_file_root + image_file,
            goinfre_root + new_image_dir,
            goinfre_root + old_image_dir)

    generate_image_file(classes_file,
            goinfre_root + new_image_dir,
            res_images_file_root + image_file)


#generate the file containing all the images paths and their classes ID
# classes_file:     the path to the file that contains all the classes names,
# images_dir:       the path to the dir that contains all images
# res_file:         the file where you write all images path and classes
def generate_image_file(classes_file, images_dir, res_file="."):
    import os

    classes = open(classes_file).readlines()
    for i in range(len(classes)):
        classes[i] = classes[i].split(",")[0].split("\n")[0].replace(" ",
                "_").replace("'","")

    image_file = open(res_file, 'w+')

    for curdir, dirnames, filenames in os.walk(images_dir):
        class_name = curdir.split("/")[-1]
        if class_name in classes:
            class_id = classes.index(class_name)

            for filename in filenames:
                image_file.write(os.path.join(curdir, filename) + " {}\n".format(class_id))

#generate a directory architecture from ilsvrc
# classes_file:     the path to the file that contains all the classes names,
# images_file:      the path to the file that contains all the images path,
# res_path:         the path where you want to stock the new datas
def generate_from_ilsvrc(classes_file, images_file, res_path, images_path=""):
    import os

    classes = open(classes_file).readlines()
    
    for i in range(len(classes)):
        classes[i] = classes[i].split(",")[0].split("\n")[0]
        os.makedirs(res_path + "/{}".format(classes[i]))
    
    list_generator = map(lambda s: s.strip().split(" "), open(images_file).readlines())
    
    for name, id in list_generator:
        os.link(images_path + "/{}".format(name),
                res_path + "/{}/{}".format(classes[int(id)], name))
