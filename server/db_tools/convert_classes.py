import os

#Remove from to_remove from old classes and add the classes from new_images

to_remove = """maypole
Model T
bolo tie, bolo, bola tie, bola
breastplate, aegis, egis
cuirass
megalith, megalithic structure
triceratops
earthstar
wreck
pickelhaube
totem pole
jinrikisha, ricksha, rickshaw
guillotine
Petri dish
space bar
Tibetan mastiff
Bedlington terrier
otterhound, otter hound
sundial
thresher, thrasher, threshing machine
shield, buckler
worm fence, snake fence, snake-rail fence, Virginia fence
trilobite
balance beam, beam
horse cart, horse-cart
nematode, nematode worm, roundworm
flatworm, platyhelminth
chambered nautilus, pearly nautilus, nautilus
swab, swob, mop
slide rule, slipstick
spotted salamander, Ambystoma maculatum
tench, Tinca tinca
bulbul
chickadee
water ouzel, dipper
common newt, Triturus vulgaris
eft
pinwheel
axolotl, mud puppy, Ambystoma mexicanum
American chameleon, anole, Anolis carolinensis
chiton, coat-of-mail shell, sea cradle, polyplacophore
toy poodle
Sealyham terrier, Sealyham
komondor
Dandie Dinmont, Dandie Dinmont terrier
kuvasz
Scottish deerhound, deerhound
EntleBucher
English foxhound
mailbag, postbag
"""

l = to_remove.split("\n")

for i in range(len(l)):
    l[i] = l[i].strip().split(",")[0]

f_in = open("/home/zeletochoy/eyecansee/server/classes", "r")

classes = f_in.readlines()
result = []

for i in range(len(classes)):
    if not classes[i].strip().split(",")[0] in l:
        result.append(classes[i].strip().split(",")[0])

for class_name in os.listdir("/sgoinfre/eyecansee/new_images_resized"):
    result.append(class_name)

f_out = open("/home/zeletochoy/eyecansee/server/new_classes", "w")

for elem in result:
    f_out.write(elem + "\n")
