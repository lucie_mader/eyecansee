import hashlib
import uuid
import pickle
import os
import mail_sender

users = {}
if os.path.isfile("users.pkl"):
    with open("users.pkl", "rb") as f:
        users = pickle.load(f)

pending = {}
if os.path.isfile("pending.pkl"):
    with open("pending.pkl", "rb") as f:
        pending = pickle.load(f)

def is_admin(admin_pass):
    sha = hashlib.sha1(admin_pass.encode("ascii")).hexdigest()
    return sha == "a851acce32f908ddaf2427f792efcc8e13a26d74"

def gen_token():
    uid = str(uuid.uuid4())
    sha = hashlib.sha1(uid.encode("ascii")).hexdigest()
    return sha[:12]

def get_token(user, admin_pass):
    if not is_admin(admin_pass):
        return "nope."
    if user in users:
        return users[user]
    token = gen_token()
    save_token(user, token)
    return token

def save_token(user, token):
    users[user] = token
    with open("users.pkl", "wb") as f:
        pickle.dump(users, f)

def del_user(user):
    del users[user]
    with open("users.pkl", "wb") as f:
        pickle.dump(users, f)

def auth(user, token):
    return users.get(user) == token

def add_pending(user, email):
    pending[user] = email
    with open("pending.pkl", "wb") as f:
        pickle.dump(pending, f)

def del_pending(user):
    del pending[user]
    with open("pending.pkl", "wb") as f:
        pickle.dump(pending, f)

def validate_pending(user):
    if not user in pending:
        return "User doesn't exist"
    token = gen_token()
    mail = pending[user]
    save_token(user, token)
    mail_sender.send_mail(mail, token, user)
    del_pending(user)
    return token
