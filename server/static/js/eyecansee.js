function fade(obj, data, time)
{
  obj.fadeOut(time, function()
  {
    obj.text(data);
    obj.fadeIn(time);
  });
}

function add_pending()
{
  if ($("#user").val().length == 0)
  {
    alert("The user field is required");
    return;
  }
  if ($("#mail").val().length == 0)
  {
    alert("The mail field is required");
    return;
  }
  alert("JS function called");
  var user = $("#user").val();
  var data =
  {
    user: user,
    mail: $("#mail").val()
  };
  $.post("/eyecansee/add_pending", data, function(data)
  {
      fade($("#form"), "Token requested for " + user, 1000);
  });
}
