from browser import document as doc, window as win
from browser import ajax, html
import time
import json
from javascript import JSConstructor
FileReader = JSConstructor(win.FileReader)

def post(url, data, callback, timeout=1):
    req = ajax.ajax()
    req.bind('complete', callback)
    req.set_timeout(timeout, timeout_display)
    req.open('POST', url, True)
    req.set_header('content-type','application/x-www-form-urlencoded')
    req.send(data)

def exportjs(func):
    setattr(win, func.__name__, func)
    return func

def timeout_display():
    doc["form"].text = "Timeout"

def update_form(req):
    doc["form"].html = req.text

def form_image(req):
    data = json.loads(req.text)
    doc["form"].html = ""
    doc["form"].style = {"text-align": "center"}
    doc["form"] <= html.BR()
    doc["form"] <= html.IMG(src="data:image/png;base64,"+data["image"], Class="img-rounded")
    doc["form"] <= html.BR() + html.BR()
    doc["form"] <= html.H3(data["text"])
    doc["form"] <= html.BR()
    doc["form"] <= html.AUDIO(src="data:audio/mp3;base64,"+data["audio"], autoplay="true")

def remove_div(name):
    del doc[name]

def print_response(res):
    res = res.text
    if res != "" and res != "OK":
        win.alert(res)

@exportjs
def validate(user):
    try:
        admin_pass = doc["admin_pass"].value
        data = {"user": user, "admin_pass": admin_pass}
        post("/eyecansee/api/validate_pending", data, print_response)
        remove_div("div-" + user)
    except Exception as e:
        print("Exception in validate:", e)

@exportjs
def nuke(user):
    try:
        admin_pass = doc["admin_pass"].value
        data = {"user": user, "admin_pass": admin_pass}
        post("/eyecansee/api/nuke_pending", data, print_response)
        remove_div("div-" + user)
    except Exception as e:
        print("Exception in validate:", e)

@exportjs
def add_pending():
    creds = {s: doc[s].value for s in ("user", "email")}
    post('/eyecansee/add_pending', creds, update_form)

@exportjs
def upload_image():
    try:
        user = doc["user"].value
        token = doc["token"].value
        path = doc["image"].value
        data = {"user": user, "token": token, "image": img}
        post("/eyecansee/api/validate_pending", data, print_response)
        remove_div("div-" + user)
    except Exception as e:
        print("Exception in validate:", e)

@exportjs
def set_path():
    doc["file"].value = doc["image"].files[0].name

@exportjs
def upload_img():
    doc["try_button"].html = "<img src='/static/img/loading.gif' height='20px'/>"
    f = doc["image"].files[0]
    fr = FileReader()
    fr.onload = post_img
    fr.readAsDataURL(f)

def post_img(ev):
    b64 = ",".join(ev.target.result.split(",")[1:])
    data = {"image": b64, "user": doc["user"].value, "token": doc["token"].value}
    post("/eyecansee/try", data, form_image, 300)
