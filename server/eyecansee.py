import matplotlib
matplotlib.use('Agg')
import sys
if not "/home/zeletochoy/caffe/python" in sys.path:
    sys.path.append("/home/zeletochoy/caffe/python")
if not "/usr/local/lib/python3.4/site-packages" in sys.path:
    sys.path.append("/usr/local/lib/python3.4/site-packages")
import json
import matplotlib.pyplot as plt
from io import StringIO
import numpy as np
import scipy.ndimage as nd
from PIL import Image
from google.protobuf import text_format
import caffe
from datetime import datetime
import os
import sentence

model_path = '/home/zeletochoy/caffe/models/bvlc_googlenet/' # substitute your path here
net_fn   = model_path + 'deploy.prototxt'
param_fn = model_path + 'bvlc_googlenet.caffemodel'

net = caffe.Classifier(net_fn, param_fn,
                       mean = np.float32([104.0, 116.0, 122.0]), # ImageNet mean, training set dependent
                       channel_swap = (2,1,0)) # the reference model has channels in BGR order instead of RGB

def preprocess(net, img):
    return np.float32(np.rollaxis(img, 2)[::-1]) - net.transformer.mean['data']

def deprocess(net, img):
    return np.dstack((img + net.transformer.mean['data'])[::-1])

zones = {"left": (0, 56, 224, 280),
         "right": (112, 56, 336, 280),
         "up": (56, 0, 280, 224),
         "down": (56, 112, 280, 336),
         "leftup": (0, 0, 224, 224),
         "rightup": (112, 0, 336, 224),
         "leftdown": (0, 112, 224, 336),
         "rightdown": (112, 112, 336, 336)}

def zones_top5(img):
    crops = []
    znames = ("left", "right", "up", "down", "leftup", "rightup", "leftdown", "rightdown")
    for z in znames:
        crops.append(np.asarray(img.crop(zones[z])))
    crops.append(np.asarray(img.resize((224, 224), Image.ANTIALIAS)))

    caffe.set_mode_gpu()
    prediction = net.predict(crops)
    prediction = [reversed(sorted(enumerate(p), key=lambda x: x[1])) for p in prediction]
    return {z: list(prediction[i])[:5] for i, z in enumerate(znames + ("all",))}

def detect(picture):
    img = Image.open(picture)

    if img.size != (336, 336):
        return "Your app is too old, please update."

    save_dir = datetime.now().strftime("%d-%m-%y_%H:%M:%S:%f")
    try:
        os.mkdir("images/" + save_dir)
    except:
        pass

    img.save("images/{}/image.jpg".format(save_dir))
    
    pred = zones_top5(img)
    text = sentence.multitext(pred)

    with open("images/{}/text.out".format(save_dir), "w") as f:
        f.write(text)
    return text
