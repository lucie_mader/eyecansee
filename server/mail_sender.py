import smtplib
import email.utils
from email.mime.text import MIMEText as MailText

from mail_setup import *

def send_mail(mail_adr, token, user_name = ''):
    message = MailText(message_body(token, user_name))
    message['Subject'] = mail_subject
    message['From'] = email.utils.formataddr((from_name, from_mail))


    server = smtplib.SMTP(smtp_server)
    try:
        server.starttls()
        server.login(from_mail, mail_pwd)
        server.sendmail(from_mail, mail_adr, message.as_string())
    except smtplib.SMTPException:
        print("Error: Unable to send mail")
    server.quit

#header injection change the FROM but subject is not set
#message = """From: EyeCanSee <noreply@eyecansee.com>
#Subject:%s\n\n%s""" % ("This is the subject", "Hello")
