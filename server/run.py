#!/usr/bin/env python3

from flask import Flask, request, render_template
import eyecansee
from base64 import b64decode, b64encode
from io import BytesIO
import uuid
import json
import sing
import tokens
import resize
from validate_email import validate_email
from collections import namedtuple

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/api/detect", methods=["POST"])
def detect():
    creds = get_creds(request)
    try:
        user, token = creds
        if not tokens.auth(user, token):
            return "Authentication failed"
        print("Request from " + user)
    except:
        return creds # error message
    data = request.form['image']
    data = b64decode(data)
    f = BytesIO(data)
    text = eyecansee.detect(f)
    audio = b64encode(sing.sing(text, "Paul")).decode("ascii")
    return json.dumps({"text": text, "audio": audio})

@app.route("/request_token")
def token():
    return render_template("request_token.html")

User = namedtuple("User", ("name", "email"))

@app.route("/validate")
def validate():
    pending = [User(name, mail) for name, mail in tokens.pending.items()]
    return render_template("validate.html", pending=pending)

@app.route("/add_pending", methods=["POST"])
def add_pending():
    try:
        user = request.form["user"]
        email = request.form["email"]
        if not validate_email(email):
            return "Invalid email address"
        tokens.add_pending(user, email)
        return "<span>Token requested for user {} with email {}.</br>" \
               "Once validated your token will be sent at this address.</span>" \
               .format(user, email)
    except Exception as e:
        print("Exception in add_pending:", e)
        return "Invalid request"

@app.route("/api/token", methods=["POST"])
def api_token():
    if not "user" in request.form:
        return "No user specified"
    if not "admin_pass" in request.form:
        return "No admin password provided"
    user = request.form["user"]
    admin_pass = request.form["admin_pass"]
    return tokens.get_token(user, admin_pass)

@app.route("/api/check_creds", methods=["POST"])
def check_creds():
    creds = get_creds(request)
    try:
        user, token = creds
        return "1" if tokens.auth(user, token) else "0"
    except:
        return creds # error message

@app.route("/api/validate_pending", methods=["POST"])
def validate_pending():
    if not "user" in request.form:
        return "No user specified"
    if not "admin_pass" in request.form:
        return "No admin password provided"
    if not tokens.is_admin(request.form["admin_pass"]):
        return "Wrong admin password"
    tokens.validate_pending(request.form["user"])
    return "OK"

@app.route("/api/nuke_pending", methods=["POST"])
def nuke_pending():
    if not "user" in request.form:
        return "No user specified"
    if not "admin_pass" in request.form:
        return "No admin password provided"
    if not tokens.is_admin(request.form["admin_pass"]):
        return "Wrong admin password"
    tokens.del_pending(request.form["user"])
    return "OK"

@app.route("/try", methods=["POST", "GET"])
def try_():
    if request.method == "GET":
        return render_template("try.html")
    creds = get_creds(request)
    try:
        user, token = creds
        if not tokens.auth(user, token):
            return "Invalid credentials"
        if not "image" in request.form:
            return "No image received"
        img = BytesIO(b64decode(request.form["image"]))
        img = resize.normalize(img)
        text = eyecansee.detect(img)
        img.seek(0)
        img = b64encode(img.read()).decode("ascii")
        audio = b64encode(sing.sing(text, "Paul")).decode("ascii")
        return json.dumps({"text": text, "image": img, "audio": audio})
    except Exception as e:
        return "Exception occured: {}".format(e)

def get_creds(request):
    if not "user" in request.form:
        return "No user specified"
    if not "token" in request.form:
        return "No token provided"
    user = request.form["user"]
    token = request.form["token"]
    return user, token

if __name__ == "__main__":
    while True:
        try:
            app.run(host='0.0.0.0', debug=True)
        except:
            pass
