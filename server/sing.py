import requests
import time
import json
from collections import defaultdict

def sing(text, voice="Melodine"):
    url = "http://voxygen.fr/sites/all/modules/voxygen_voices/assets/proxy/index.php?method=redirect"
    res = requests.get(url, params={"ts": int(time.time()), "text": text, "voice": voice})
    return res.content

def get_voices():
    url = "http://voxygen.fr/voices.json"
    res = requests.get(url)
    voices = defaultdict(list)
    for lang in yml["groups"]:
        for v in lang["voices"]:
            voices[lang["code"]].append(v["name"])
    return voices
