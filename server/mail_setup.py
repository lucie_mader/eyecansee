from_mail = "me@example.com"
mail_pwd = "password_for_from_mail"
from_name = "NameYouWantDisplayed"

smtp_server = "smtp.example.com:25"

mail_subject = "Your token for EyeCanSee"

def message_body(token, user_name):
    return "Hi {0},\n\n" \
    "Thanks for trying out EyeCanSee, here are your username and token to use the app:\n\n" \
    "usename: {0}\n" \
    "token: {1}\n\n" \
    "Don't lose those, we don't have a recovery system yet!\n\n" \
    "To install the application, go to http://scia.epita.fr/eyecansee/ with your\n" \
    "android smartphone and click on Download.\n\n" \
    "-- \nThe EyeCanSee team".format(user_name, token)
