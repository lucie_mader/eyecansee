import yaml
from collections import defaultdict

classes = None
with open("classes") as f:
    classes = f.read().split("\n")

yml = None
with open("hierarchy.yml") as f:
    yml = yaml.load(f.read())
hierarchy = {val: cat for cat, lst in yml.items() for val in lst}

def friendlytext(idx):
    best = classes[idx].split(', ')[0].lower()
    name = hierarchy.get(classes[idx])

    text = 'an' if best[0] in 'aeiou' else 'a'
    text += ' ' + best

    return text

side_dic = {"leftup": "top left", "leftdown": "bottom left",
"rightup": "top right", "rightdown": "bottom right", "down": "bottom",
"up": "top", "all": "middle", "left": "left", "right": "right"}

def multitext(dict):
    threshold = 0.5
    image_size = 336

    res_dict = defaultdict(lambda: ('none', threshold)) 

    for pos, value in dict.items():
        for elem, proba in value:
            if proba > res_dict[elem][1]:
                res_dict[elem] = (pos, proba)

    l = []
    file_str = ""

    for obj, values in filter(lambda tup: tup[1][0] != 'none',
        res_dict.items()):
            file_str = friendlytext(obj).rstrip('.')
            if res_dict[obj][0] != 'all':
                file_str += " on the {}".format(side_dic[res_dict[obj][0]])
            else:
                file_str += " in the {}".format(side_dic[res_dict[obj][0]])
            l.append(file_str)

    if len(l) > 1:
        text = "I can see {} and {}.".format(", ".join(l[:-1]), l[-1])
    elif len(l) == 1:
        text = "I can see {}.".format(l[0])
    else:
        text = "I can't see anything"

    return text
